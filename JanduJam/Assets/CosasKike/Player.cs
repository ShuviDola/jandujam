﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
using UnityEngine.SceneManagement;

public class Player : MonoBehaviour
{


    [SerializeField]
    private float velX = 4;
    [SerializeField]
    private float foceJump = 4;
    private Rigidbody2D rigidbody;
    
    public Animator animator;
    private bool isGround;
    public GameObject espada;

    [SerializeField]
    private float run = 2;
    private bool isRun;
    private bool walk;
    private bool attack;
    private bool attackRun;
    private bool walkAttack;
    private bool jumpAttack;
    private bool jump;
    private bool dead;
    public Scrollbar vida;

    public AudioClip attackSound;
    public AudioClip moneda;
    public AudioClip deadSound;
    AudioSource audio;
    bool reproducir = false;

    int monedaSecreto;

    public GameObject secreto;
    // Start is called before the first frame update
    void Start()
    {
        monedaSecreto = PlayerPrefs.GetInt("Moneda");
        isRun = false;
        walk = false;
        attack = false;
        attackRun = false;
        jump = false;
        jumpAttack = false;
        walkAttack = false;
        dead = false;
        vida.size = PlayerPrefs.GetFloat("Vida");
        animator = GetComponent<Animator>();
        audio = GetComponent<AudioSource>();
        rigidbody = GetComponent<Rigidbody2D>();
    }

    // Update is called once per frame
    void Update()
    {
        if(!dead){
            float move = Input.GetAxis("Horizontal");
            
            if(Input.GetAxis("Run") > 0 && isGround){
                run = 2;
                isRun = true;
            } else {            
                isRun = false;
                run = 1;
            }

            if(move > 0){
                transform.localRotation = Quaternion.Euler(0, 0, 0);
                transform.Translate(velX*run*Time.deltaTime,0,0);
                walk = true;
            } else if(move < 0){
                transform.localRotation = Quaternion.Euler(0, 180, 0);
                transform.Translate(velX*run*Time.deltaTime,0,0);
                walk = true;
            } else{

                walk = false;
            }

            

            if(isGround){
                jump = false;
                jumpAttack = false;

                if(Input.GetAxis("Attack") > 0 && isRun){
                    espada.SetActive(true);
                    attackRun =true;
                } else if(Input.GetAxis("Attack") > 0 && move == 0){
                    espada.SetActive(true);
                    attack =true;
                } else if(Input.GetAxis("Attack") > 0 && move != 0){
                    espada.SetActive(true);
                    walkAttack =true;
                } else{
                    espada.SetActive(false);
                    attack = false;
                    attackRun = false;
                    walkAttack = false;
                }



            } else {
                isRun = false;
                if(Input.GetAxis("Attack") > 0){
                    espada.SetActive(true);
                    jumpAttack = true;
                }
            }

            if(Input.GetAxis("Attack") > 0 && !reproducir){
                reproducir = true;
                InvokeRepeating("soundAtta", 0.1f, 0.3f);
            }
            if(Input.GetAxis("Attack") <= 0){
                CancelInvoke();
                reproducir = false;
            }

            animator.SetBool("Walk", walk);
            animator.SetBool("Jump", jump);
            animator.SetBool("Run", isRun);
            animator.SetBool("RunAttack", attackRun);
            animator.SetBool("Attack", attack);
            animator.SetBool("WalkAttack", walkAttack);
            animator.SetBool("JumpAttack", jumpAttack);
        } else {
            
            animator.SetBool("Dead", dead);
            Invoke("muere", 1.8f);
        }

        if(monedaSecreto <= 13){
            secreto.SetActive(true);

        }

    }
    
    void muere(){
        SceneManager.LoadScene("MainMenu");
    }

    void soundAtta(){
        audio.clip = attackSound;
        audio.Play();
    }

    void FixedUpdate()
    {   
        if(Input.GetAxis("Jump") > 0 && isGround){
            rigidbody.AddForce(transform.up * foceJump, ForceMode2D.Impulse);
            jump = true;
            isGround = false;
        }
        
    }

    void OnTriggerEnter2D(Collider2D other)
    {
        if(other.gameObject.tag == "Ground"){
            isGround = true;
        }

        if(other.gameObject.tag == "Enemy"){
            Enemy e = other.gameObject.GetComponent<Enemy>();
            if(!e.isDead()){
                vida.size -= 0.2f;
                if(vida.size <= 0){
                    audio.clip = deadSound;
                    audio.Play();
                    dead = true;   
                }
            }
            
        }

        if(other.gameObject.tag == "Magia"){
            vida.size -= 0.2f;
            if(vida.size <= 0){
                audio.clip = deadSound;
                audio.Play();
                dead = true;   
            }
            
        }

        if(other.gameObject.tag == "Lava"){
            dead = true;
            audio.clip = deadSound;
                audio.Play();
        }

        if(other.gameObject.tag == "Coin"){
            audio.clip = moneda;
            audio.Play();
            monedaSecreto++;
            Destroy(other.gameObject);
        }

        
    }


    void OnTriggerStay2D(Collider2D other)
    {
        if(other.gameObject.tag == "Ground"){
            isGround = true;
        }

        if(other.gameObject.tag == "Puerta"){
            PlayerPrefs.SetFloat("Vida", vida.size);
            SceneManager.LoadScene("Level2");
            
        }
    }
}
