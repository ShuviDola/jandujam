﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Enemy : MonoBehaviour
{

    public GameObject puntA;
    public GameObject puntB;
    public Animator animator;
    
    private bool walkAttack;
    private bool attack;
    private bool dead;
    [SerializeField]
    private float speed = 3;
    [SerializeField]
    private bool direccion;

    AudioSource audio;

    public AudioClip deadSound;
    // Start is called before the first frame update
    void Start()
    {
        dead = false;
        walkAttack = false;
        attack = false;
        animator = GetComponent<Animator>();
        audio = GetComponent<AudioSource>();
    }

    // Update is called once per frame
    void Update()
    {
        if(!dead){
            if(transform.position.x > puntA.transform.position.x && direccion){
                transform.localRotation = Quaternion.Euler(0, 180, 0);
                transform.Translate(speed*Time.deltaTime,0,0);

            } else if(transform.position.x < puntA.transform.position.x){
                direccion = false;
            }
            
            if(transform.position.x < puntB.transform.position.x && !direccion){
                transform.localRotation = Quaternion.Euler(0, 0, 0);
                transform.Translate(speed*Time.deltaTime,0,0);
            }else if(transform.position.x > puntB.transform.position.x){
                direccion = true;
            }



            animator.SetBool("WalkAttack", walkAttack);
            animator.SetBool("Attack", attack);
            animator.SetBool("Walk", true);
        } else {
            dead = true;
            animator.SetBool("Dead", dead);
            Invoke("muere", 1f);
        }
    }


    void OnCollisionEnter2D(Collision2D other) {
        if(other.gameObject.tag == "Espada"){
            dead = true;
            audio.clip = deadSound;
            audio.Play();
        }
    }
    void OnTriggerEnter2D(Collider2D other)
    {
        if(other.tag == "Espada"){
            dead = true;
            audio.clip = deadSound;
            audio.Play();
        }
    }

    void muere(){
        Destroy(this.gameObject);
    }

    public bool isDead(){
        return dead;
    }
}
