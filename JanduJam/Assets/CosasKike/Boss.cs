﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Boss : MonoBehaviour
{

    public Animator animator;
    
    private float tiempoEntreAtaques;
    private float ataque;
    private bool dead;
    private bool attack;

    public GameObject attack1;
    public GameObject attack2;

    public AudioClip attackSound;
    public AudioClip deadSound;
    AudioSource audio;

    // Start is called before the first frame update
    void Start()
    {
        dead = false;
        attack= false;
        animator = GetComponent<Animator>();
        audio = GetComponent<AudioSource>();
        tiempoEntreAtaques = 5f;
        ataque = Time.time;
    }

    // Update is called once per frame
    void Update()
    {
        if(!dead){
            if(ataque+tiempoEntreAtaques < Time.time){
                Debug.Log("entra");
                int random = Random.Range(0, 10);
                if(random < 9){
                    GameObject bullet = Instantiate(attack1);
                    bullet.transform.position = new Vector3(transform.position.x - .4f, transform.position.y - .2f, transform.position.y);
                } else {
                    GameObject bullet = Instantiate(attack2);
                    bullet.transform.position = new Vector3(transform.position.x - .4f, transform.position.y - .2f, transform.position.y);
                }

                
                    attack = true;
                    ataque = Time.time;
                    Invoke("paraAnim", 0.5f);
                    audio.clip = attackSound;
                    audio.Play();
                }

                animator.SetBool("Attack", attack);
            } else {
                dead = true;
                animator.SetBool("Dead", dead);
            Invoke("muere", 1f);
        }
    }

    void OnTriggerEnter2D(Collider2D other)
    {
        if(other.tag == "Espada"){
            dead = true;
            audio.clip = deadSound;
            audio.Play();
        }
    }

    void muere(){
        Destroy(this.gameObject);
    }

    void paraAnim(){
        attack = false;
    }
}
