﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class EnemyPersegidor : MonoBehaviour
{

    public GameObject puntA;
    public GameObject puntB;
    public Animator animator;
    
    private bool attack;
    private bool walk;
    [SerializeField]
    private float speed = 1;
    [SerializeField]
    private bool direccion;
    // Start is called before the first frame update


    //---------------------

    private GameObject player;

    void Start()
    {
        walk = false;
        attack = false;
        animator = GetComponent<Animator>();
        player = GameObject.FindGameObjectWithTag("Player");
    }

    // Update is called once per frame
    void Update()
    {
        if((transform.position.x - player.transform.position.x) < 20 && (transform.position.x - player.transform.position.x) > -20){

            if((transform.position.x - player.transform.position.x) > -1f){
                transform.localRotation = Quaternion.Euler(0, 180, 0);
                transform.Translate(speed*Time.deltaTime,0,0);
            walk = true;
                
            } else if ((transform.position.x-player.transform.position.x) < 1){
                transform.localRotation = Quaternion.Euler(0, 0, 0);
                transform.Translate(speed*Time.deltaTime,0,0);
            walk = true;
            } else {
                attack = true;
            }
             
        } else {
            walk = false;

        }

        animator.SetBool("Attack", attack);
        animator.SetBool("Walk", walk);
    }
}
